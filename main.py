import os
from datetime import datetime

def testare(cuv: str, test_case: str) -> bool|str:
    match test_case:
        case "incepe_cu_litera_mare":
            return cuv[0].isupper()
        case "incepe_cu_litera_mica":
            return cuv[0].islower()
        case "numar_vocale_mai_mare_decat_numar_consoane"|"numar_consoane_mai_mare_decat_numar_vocale":
            voc, cons = 0, 0
            for l in cuv:
                if l.isalpha():
                    if l in "aeiouAEIOU":
                        voc += 1
                    else:
                        cons += 1
            
            res = (voc > cons) if test_case == "numar_vocale_mai_mare_decat_numar_consoane" else (voc < cons)
            return res
        case "lungime_mai_mare_de_10_caractere":
            return len(cuv) > 10
        case "capitalizare":
            while not cuv[0].isalpha():
                cuv = cuv[1:]
            return cuv[0].isupper()
        case "contine_numere":
            return(any(l.isdigit() for l in cuv))
        case "contine_caractere_speciale":
            return any(not(l.isalnum() or l.isspace()) for l in cuv)
        case "este_palindrom":
            return cuv == cuv[::-1]
        case "are_litere_alternante":
            prev_type = None
            for l in cuv:
                if not l.isalpha():
                    continue
                curr_type = "v" if l in "aeiouAEIOU" else "c"
                
                if curr_type == prev_type:
                    return False
                
                prev_type = curr_type
                
            return True
        case _:
            return "Ceva nu a mers bine!"

def main() -> None:
    lista_cuvinte: str = "cuvinte.txt"
    test_path: str = "teste/"
    tests_list: list = ["incepe_cu_litera_mare", "incepe_cu_litera_mica", "numar_vocale_mai_mare_decat_numar_consoane", "numar_consoane_mai_mare_decat_numar_vocale", "lungime_mai_mare_de_10_caractere", "capitalizare", "contine_numere", "contine_caractere_speciale", "este_palindrom", "are_litere_alternante"]
    
    nume_testor = input("Introduceti numele testorului: ").strip()
    
    while nume_testor == '':
        print("Numele nu poate fi gol!")
        nume_testor = input("Introduceti numele testorului: ").strip()
    
    if not os.path.isfile(lista_cuvinte):
        print("Nu avem cuvinte de testat!")
        return
    
    if os.path.exists(test_path):
        print(f"Exista teste anterioare gasite in folderul {os.path.dirname(os.path.abspath(__file__)) + test_path} ! Doriti sa continuati? (y/n)")
        ans = input()
        if ans.lower() == 'y':
            os.system(f"rm -r {test_path}")
        else:
            print("Nu am putut continua!")
            return
    
    with open(lista_cuvinte, "r") as f:
        cuv:list[str] = f.readlines()
        
    if not os.path.exists(test_path):
        os.mkdir(test_path)

    for c in cuv:
        c = c.strip()
        for test in tests_list:
            if not os.path.exists(test_path+test):
                os.mkdir(test_path+test)

            with open(f"{test_path+test}/{c}.txt", "w") as f:
                now = datetime.now()
                data = now.date()
                ora = now.hour
                res: bool|str = testare(c, test)
                if res == "Ceva nu a mers bine!":
                    f.write(f"{nume_testor}, {data}, {ora}, {c} - Error: {res}")
                else:
                    f.write(f"{nume_testor}, {data}, {ora}, {c} - {'OK' if testare(c, test) else 'NOT OK'}")
                
    print(f"\n✅ {nume_testor}: Am testat {len(cuv)} cuvinte!")
            
            

if __name__ == "__main__":
    main()